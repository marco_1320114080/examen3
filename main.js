var monto = document.getElementById('monto');
var tiempo = document.getElementById('tiempo');
var interes = document.getElementById('interes');
var btnCalcular = document.getElementById('btnCalcular');
var llenarTabla = document.querySelector('#lista-tabla tbody');

btnCalcular.addEventListener('click', () => {
    calcularCuota(monto.value, interes.value, tiempo.value);
})

function calcularCuota(monto, interes, tiempo){

    while(llenarTabla.firstChild){
        llenarTabla.removeChild(llenarTabla.firstChild);
    }

    let fechas = [];
    let fechaActual = Date.now();
    let mes_actual = moment(fechaActual);
    mes_actual.add(1, 'month');

    let pagoInteres=0, pagoCapital = 0, cuota = 0;

    cuota = monto * (Math.pow(1+interes/100, tiempo)*interes/100)/(Math.pow(1+interes/100, tiempo)-1);
    //Amortizacion
    var Pi= monto/tiempo;

    for(let i = 1; i <= tiempo; i++) {

        //Fi=-monto + comision inicial (1 + iva)

        pagoInteres = parseFloat(monto*(interes/100));
        pagoCapital = cuota - pagoInteres;
        monto = parseFloat(monto-pagoCapital);

        var comision = monto * 0.01;


        //Formato fechas
        fechas[i] = mes_actual.format('DD-MM-YYYY');
        mes_actual.add(1, 'month');

        //Interes ((saldo insoluto*0.18)/360*30)
        var inte = (monto*0.18)/360*30;

        //iva    (inte * 0.16)
        var iva = inte*0.16;

        //Flujo
        var flujo=0;
        flujo=inte+iva+Pi;

        const row = document.createElement('tr');
        row.innerHTML = `
        <td>${[i]}-------|</td>
        <td>${fechas[i]}|</td>
        <td>[---Vacio---]</td>
        <td>----${monto.toFixed(2)}|</td>
        <td>----${Pi.toFixed(2)}|</td>
        <td>--------${inte.toFixed(2)}|</td>
        <td>----${iva.toFixed(2)}|</td>
        <td>----${flujo.toFixed(2)}|</td>
        `;
        llenarTabla.appendChild(row);
    }
}